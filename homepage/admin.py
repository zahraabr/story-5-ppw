from django.contrib import admin
from .models import Friends, ClassYear

# Register your models here.
admin.site.register(Friends)
admin.site.register(ClassYear)
