from django.shortcuts import render
from django.shortcuts import redirect
from .models import Friends, ClassYear
from . import forms


# Create your views here.
def story3(request):
    return render(request, 'story3.html')

def portfolio(request):
	return render(request, 'portfolio.html')

def contact(request):
	return render(request, 'contact.html')
	

def addFriend(request):
    if request.method == 'POST':
        form = forms.FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:friend')
    else:
        form = forms.FriendForm()
    return render(request, 'addFriend.html', {'form': form})

def friend(request):
    friends = Friends.objects.all()
    classyear = ClassYear.objects.all()
    response = {'friends' : friends, 'classyear': classyear}
    return render(request, 'friend.html', response)

def deleteFriend(request, name):
    friends = Friends.objects.get(name=name)
    friends.delete()
    return redirect('/friend/')
