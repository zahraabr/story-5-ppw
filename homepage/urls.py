from django.urls import path
from . import views
from django.contrib import admin


app_name = 'homepage'

urlpatterns = [
    path('', views.story3, name='story3'),
    path('profile/', views.portfolio, name='portfolio'),
    path('contact/', views.contact, name='contact'),
	path('friend/', views.friend, name='friend'),
	path('addFriend/', views.addFriend, name='addFriend'),
    path('friend/deleteFriend/<str:name>', views.deleteFriend, name='deleteFriend'),
]